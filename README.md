# Guia Gitlab

## Índice
*   [Commits](#commits)
*   [Issues](#issues)
*   [Labels](#labels)
*   [Boards](#boards)
*   [Milestones](#milestones)

## Commits  
O título do commit deve dizer com a maior precisão possível, de preferência em 50 caracteres, o que aquele commit modifica no projeto. O texto deve ser escrito em 3ª pessoa, com sujeito oculto e com referência ao Id da Issue logo após #.  Após o título deverá haver uma linha em vazia e a partir da terceira deverá haver as descrições das principais modificações.  
Ex: 
> "Cria CRUD de associados, closes #7  
>   
> &ast; upload de imagem de perfil  
> &ast; README atualizado  
> &ast; add adminlte"  

Tu poderás referenciar as issues através do número da issue. Ex.: #1  

**Obs.:** *closes*, *close*, *closed*, *fix*, *fixes*,  *fixed* são usados para fechar a issue referenciada quando o commit for incorporado (merge) à *master*. Caso queira apenas referenciar sem fechar a issue ao dar merge use apenas o *#id* sem essas palavras chaves.  

## Issues
Uma estória de usuário pode ser caracterizada como uma curta e simples descrição da necessidade do cliente. Ela normalmente é contada a partir da perspectiva de quem precisa da nova necessidade sendo, geralmente, um usuário, cliente do sistema. Uma estória de usuário deve explicar bem para quem, o que e por que está sendo criada. 
Isso é importante, principalmente, para desenvolvedores que poderão construir softwares entendendo os objetivos e necessidades do cliente de forma mais rápida.
#### Conteúdo
Uma estória deve atender a 4 características principais:

*  **Independente**: Uma estória deve ser completa em seu escopo, e não deve depender de outra para ser concluída.
*  **Estimável**: Deve ser possível prever a duração do desenvolvimento de uma estória. Se isso não for possível, deve-se analisar o impedimento, tratá-lo e então avaliar a estória.
*  **Pequena**: Uma estória não deve ter um tempo estimável maior do que 1 Sprint. Se isso ocorrer, é porque essa estória precisa ser dividida em outras estórias.
*  **Testável**: Uma estória deve ser testável, ou seja, dada uma entrada, deve haver uma saída.

Exemplo:

> **Título:**  
Como usuário, gostaria de me cadastrar no sistema.  
**Descrição**:  
O formulário de cadastro deve conter os campos Nome, Email, Telefone, Senha e Confirmação de senha e o botão CADASTRAR. A validação dos campos é obrigatória.  

#### Pesos
O Gitlab possui uma classificação das issues com base em pesos (desde o peso 1 até o peso 8). Issues maiores e/ou mais difíceis recebem peso maior em relação as menores e/ou mais fáceis. Uma recomendação é se usar a numeros da sequencia de fibonacci(1,2,3,5,8) como pesos.
Issues muito grandes dificultam a visualização do estado do projeto e ainda inviabilizam o paralelismo de atividades, portanto as issues devem ser adequadas para que não haja impedimentos. Dessa forma o ideal é que não existam issues com peso maior que 3, caso alguma issue exceda essa recomendação deverá ser avaliado a possibilidade de dividir esta issue em duas ou mais de pesos menores.  

#### Assinar  
Toda issue tem um campo _Assignee_ que significa a pessoa designada para concluir aquela issue, assim cada membro da empresa deve escolher a issue que irá solucionar e assiná-la para indicar que irá realizá-la, assim, caso outra pessoa assine antes, ela deterá a resonsábilidade daquela issue e os demais membros deverão optar por outra issue.  

#### Fechamento por Merge Request
Merge Request para anexar ao projeto a implementação que resolve determinada issue. Podemos criar uma branch iniciada com "_id_-" (onde _id_ é o número da issue) para relacioná-la com uma issue, assim quando fizermos Merge Request dessa branch com a master ela automaticamente fechará a issue. Lembrando sempre de alterar a label para Done após esse processo.  

#### Fechamento Manual  
Caso a branch não esteja relacionada com a issue, podemos fazer o Merge Request e fechar a issue fechar manualmente pelo painel da issue. Lembrando sempre de alterar a label para Done após esse processo.  

## Labels  
Labels são rótulos que usamos parar marcar e classificar as issues, assim melhorando a oraganização a a visualização do estado de algum projeto.  
As labels atualmente usadas são:  
 
| Label | Descrição |
| ------ | ------ |
| To-Do | Indica que a resolução  da Issue ainda não foi iniciada |
| Doing | Indica que a resolução da Issue está em desenvolvimento |
| Done  | Indica que a Issue foi resolvida |
| Gestão | Indica que a Issue é relativa a atividades de gestão |
| Desenvolvimento | Indica que a Issue é de desenvolvimento |
| DAF, DGP, DMKT, DPRE/DVP e DPRJ | Indica que a Issue é de alguma das diretorias sinalizadas |
| Discordância ( , *, **) | Indica discondância na medida de peso da issue  |  

> Existem três labels para discordância (discordância, discordância*, discordância**), a primeira indica a discordância em si, a segunda indica a justificativa ou réplica do discordado e a terceira indica o contestamento ou tréplica do discordante. Caso a discordância permaneça após os três passos, DGP (Diretoria de Gestão de Pessoas) deverá ser acionada.  
**Obs**.:  
> Toda Issue deve iniciar com a label "To-Do".

## Boards  
Boards do Gitlab é um ferramenta que nos permite acompanhar a situação de um projeto ou um grupo, além de nos permitir manipular as issues com maior dinamicidade. Boards podem ser acessados no projeto ou no grupo no menu de Issues > Board.  

Por padrão temos a Board Development que traz duas listas, as das issues abertas e das fechadas de um projeto. Essa board poderá ser personalizada para mostrar as issues em andamento (To Do e Doing) adicionando listas. Parar adicionar uma lista a um Board basta clicar em **[Add List]** e selecionar a label referente àquela lista.  

![Exemplo de Boards](https://docs.gitlab.com/ee/user/project/img/issue_board_assignee_lists.png)  

Podemos também simplesmente arrastar uma issue de uma lista para outra para alterar sua label ou seu estado (Closed por exemplo).  
Algumas outras facilidades de se utilizar Boards é poder adicionar várias issues a uma label Através do botão **[Add Issue]** e personalizar diferentes Boards para obter outra visualização do projeto ou setores (grupos) como o [Board da InfoJr](https://gitlab.com/groups/InfoJrUFBA/-/boards/767666?&label_name[]=Presid%C3%AAncia) por exemplo. 


## Milestones  
Toda Milestone intitulada pelo periodo que ela da Sprint que ela representa, por exemplo: “Milestone 2019 ABR [14 a 27]”. Atualmente usamos Sprints de 2 semanas. As issues definidas em cada Sprint devem ser concluidas até o final da Sprint, o atraso de uma Sprint não deve significar o atraso de todas as Sprints posteriores a ela. Se uma sprint atrasar, a equipe deve analisar o motivo e avaliar. 
A independência das Sprints é resultado da independência das estórias. Uma ferramenta útil para acompanhar o andamento da Sprint é o Burndown chart, que mostra em um gráfico o número de issues não conluidas daquela Milestone.  

![Exemplo de Burndown chart](https://docs.gitlab.com/ee/user/project/milestones/img/burndown_chart.png)  

O esperado é que gráfico gerado se aproxime da linha cinza, sinal de que as issues foram concluidas de forma equilibrada no decorrer da Sprint.  



